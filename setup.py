#!/usr/bin/env python

from setuptools import setup

setup(name='f3proba',
      version='1.0',
      # list folders, not files
      packages=['f3proba'],
      )
