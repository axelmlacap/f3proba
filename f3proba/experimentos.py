import numpy as np
from scipy.interpolate import interp1d

nt = 25.40  # [1]
ndc = 500.00  # [1]
# vt = 4.06  # [uV]
# vdc = 80.10  # [uV]
c = 1.602176e-1  # V


def medir_corriente(s=None):
    n = np.array(np.random.poisson(ndc, s) + np.random.normal(0, nt, s)).astype(int)
    if s is not None:
        return n
    else:
        return f"{n}"


def medir_tension(s=None):
    v = c * np.array(medir_corriente(s)).astype(float)
    if s is not None:
        return v
    else:
        return f"{v:.3f} μV"


def adquirir_traza(integration_time: float = 1.0, rate: float = 10):
    pulse_width = 0.005
    pulse_height = 5.0
    dt = pulse_width / 10
    size = int(integration_time / dt)
    t = np.arange(size) * dt
    pulse = pulse_height * np.exp(- t / pulse_width)

    # n = int(np.round(np.random.poisson(integration_time * rate)))
    interarrivals = np.array(np.random.exponential(1 / rate, 10 * int(integration_time * rate)))
    arrivals = np.cumsum(interarrivals)

    signal = np.zeros(size)
    for i, arrival in enumerate(arrivals):
        if arrival > integration_time:
            break

        index = int(arrival / dt)
        signal[index:] = pulse[0:size-index]

    arrivals = arrivals[0:i]

    return t, signal, arrivals


def contar_pulsos(integration_time: float = 1.0, rate: float = 10, size: int = 1):
    n = []
    for i in range(size):
        t, signal, arrivals = adquirir_traza(integration_time, rate)
        n.append(len(arrivals))
    n = np.array(n)

    return n


if __name__ == "__main__":
    import matplotlib.pyplot as plt

    t, signal, arrivals = adquirir_traza()
    n = len(arrivals)

    plt.plot(t, signal)
    plt.plot(arrivals, [signal.max()] * len(arrivals), linewidth=0, marker="o")

    print(f"n = {n}")
    print(contar_pulsos(size=10))
    print(contar_pulsos(size=10000).mean())
